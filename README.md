# tf-aws-gitlab-runner

A Gitlab Runner Terraform Module

Gives you:

- An ASG for a single EC2 node with gitlab runner installed and running

## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 1.x.x          | 0.12.x            |
| 0.x.x          | 0.11.x            |

## Contributing

Ensure any variables you add have a type and a description.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_template"></a> [template](#provider\_template) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_runner"></a> [runner](#module\_runner) | git::ssh://git@gitlab.com/claranet-pcp/terraform/aws/tf-aws-asg.git | v2.0.0 |

## Resources

| Name | Type |
|------|------|
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [template_file.runner_userdata](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_id"></a> [ami\_id](#input\_ami\_id) | ID of the base AMI the runner will use | `string` | n/a | yes |
| <a name="input_associate_public_ip_address"></a> [associate\_public\_ip\_address](#input\_associate\_public\_ip\_address) | Whether the instance is launched with a public IP | `bool` | `false` | no |
| <a name="input_enable_attach_eni"></a> [enable\_attach\_eni](#input\_enable\_attach\_eni) | Attach ENI to ASG launched instance, for use with private IP allocation use cases | `bool` | `false` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Whether or not to create resources | `string` | `"true"` | no |
| <a name="input_eni_availability_zones"></a> [eni\_availability\_zones](#input\_eni\_availability\_zones) | List of one or more availability zones for the group. Used for EC2-Classic, attaching a network interface via id from a launch template | `list(any)` | <pre>[<br>  ""<br>]</pre> | no |
| <a name="input_eni_private_ip_list"></a> [eni\_private\_ip\_list](#input\_eni\_private\_ip\_list) | List of private IPs to assign to the ENI in sequential order | `list(any)` | <pre>[<br>  ""<br>]</pre> | no |
| <a name="input_eni_subnet_id"></a> [eni\_subnet\_id](#input\_eni\_subnet\_id) | Subnet ID to create the ENI in | `string` | `""` | no |
| <a name="input_extra_asg_tags"></a> [extra\_asg\_tags](#input\_extra\_asg\_tags) | n/a | `list` | `[]` | no |
| <a name="input_gitlab_check_interval"></a> [gitlab\_check\_interval](#input\_gitlab\_check\_interval) | Amount of time between checks for work | `string` | `"5"` | no |
| <a name="input_gitlab_concurrent_job"></a> [gitlab\_concurrent\_job](#input\_gitlab\_concurrent\_job) | Number of concurrent tasks to run | `string` | `"1"` | no |
| <a name="input_gitlab_rct_default_ttl"></a> [gitlab\_rct\_default\_ttl](#input\_gitlab\_rct\_default\_ttl) | Minimum time to preserve a newly downloaded images or created caches | `string` | `"1m"` | no |
| <a name="input_gitlab_rct_expected_free_files_count"></a> [gitlab\_rct\_expected\_free\_files\_count](#input\_gitlab\_rct\_expected\_free\_files\_count) | How many free files (i-nodes) to cleanup | `string` | `"262144"` | no |
| <a name="input_gitlab_rct_expected_free_space"></a> [gitlab\_rct\_expected\_free\_space](#input\_gitlab\_rct\_expected\_free\_space) | How much the free space to cleanup | `string` | `"2GB"` | no |
| <a name="input_gitlab_rct_low_free_files_count"></a> [gitlab\_rct\_low\_free\_files\_count](#input\_gitlab\_rct\_low\_free\_files\_count) | When the number of free files (i-nodes) runs below this value trigger the cache and image removal | `string` | `"131072"` | no |
| <a name="input_gitlab_rct_low_free_space"></a> [gitlab\_rct\_low\_free\_space](#input\_gitlab\_rct\_low\_free\_space) | Threshold for when to trigger the cache and image removal | `string` | `"1GB"` | no |
| <a name="input_gitlab_rct_use_df"></a> [gitlab\_rct\_use\_df](#input\_gitlab\_rct\_use\_df) | Use a command line df tool to check disk space. Set to false when connecting to remote Docker Engine. Set to true when using with locally installed Docker Engine | `string` | `"1"` | no |
| <a name="input_gitlab_runner_docker_image"></a> [gitlab\_runner\_docker\_image](#input\_gitlab\_runner\_docker\_image) | Gitlab Runner default docker image. | `string` | `"terraform:light"` | no |
| <a name="input_gitlab_runner_docker_privileged"></a> [gitlab\_runner\_docker\_privileged](#input\_gitlab\_runner\_docker\_privileged) | Toggle Gitlab Runner running privileged | `string` | `"false"` | no |
| <a name="input_gitlab_runner_locked"></a> [gitlab\_runner\_locked](#input\_gitlab\_runner\_locked) | Toggle locking the Gitlab Runner to the specific project | `string` | `"true"` | no |
| <a name="input_gitlab_runner_name"></a> [gitlab\_runner\_name](#input\_gitlab\_runner\_name) | Name that the runner will appear with in GitLab | `string` | n/a | yes |
| <a name="input_gitlab_runner_tags"></a> [gitlab\_runner\_tags](#input\_gitlab\_runner\_tags) | Tags to add to the runner | `string` | `"specific,docker"` | no |
| <a name="input_gitlab_runner_token"></a> [gitlab\_runner\_token](#input\_gitlab\_runner\_token) | Token used to authenticate with CI server | `string` | `""` | no |
| <a name="input_gitlab_runner_token_param"></a> [gitlab\_runner\_token\_param](#input\_gitlab\_runner\_token\_param) | Token used to authenticate with CI server from AWS SSM parameter store | `string` | `""` | no |
| <a name="input_gitlab_runner_url"></a> [gitlab\_runner\_url](#input\_gitlab\_runner\_url) | URL of the GitLab CI server to connect to | `string` | `"https://gitlab.com/ci"` | no |
| <a name="input_iam_profile"></a> [iam\_profile](#input\_iam\_profile) | Resource ID of the IAM profile the instance will use | `string` | n/a | yes |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | AWS Instance Type to use | `string` | `"t2.small"` | no |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | Name of the SSH key to use | `string` | n/a | yes |
| <a name="input_security_groups"></a> [security\_groups](#input\_security\_groups) | List of addtional security group to apply | `list(string)` | n/a | yes |
| <a name="input_vpc_env"></a> [vpc\_env](#input\_vpc\_env) | Envrioment name of VPC. I.e. Prod, Mgmt etc. | `string` | n/a | yes |
| <a name="input_vpc_subnets"></a> [vpc\_subnets](#input\_vpc\_subnets) | List of subnets that the GitLab runner will be placed in | `list(string)` | <pre>[<br>  ""<br>]</pre> | no |

## Outputs

No outputs.
